window.onload = () => {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    const url = 'https://rickandmortyapi.com/api/character';

    const request = new Request(url, {
        method: 'GET',
        headers
    });

    fetch(request)
        .then(res => res.json())
        .then(data => {
            const rawData = data.results;
            console.log(rawData);
            rawData.map(character => {
                //all needed data is listed below as an entity
                let created = character.created;
                let species = character.species;
                let img = character.image;
                let episodes = character.episode;
                let name = character.name;
                let location = character.location.name;
                //create element
                //append element
                console.log(character);

                let table = document.getElementById("myTable");

                let tr = document.createElement("TR");
                table.appendChild(tr);

                let td0 = document.createElement('TD');
                let im = document.createElement('img');
                im.src = img;
                td0.appendChild(im);
                tr.appendChild(td0);

                let td1 = document.createElement('TD');
                td1.appendChild(document.createTextNode(name));
                tr.appendChild(td1);

                let td2 = document.createElement('TD');
                td2.appendChild(document.createTextNode(species));
                tr.appendChild(td2);

                let td3 = document.createElement('TD');
                td3.appendChild(document.createTextNode(created));
                tr.appendChild(td3);

                let td4 = document.createElement('TD');
                td4.appendChild(document.createTextNode(episodes));
                tr.appendChild(td4);

                let td5 = document.createElement('TD');
                td5.appendChild(document.createTextNode(location));
                tr.appendChild(td5);


            });
        })

        .catch((error) => {
            console.log(JSON.stringify(error));
        });
}

function sortTable(n) {
    var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
    table = document.getElementById("myTable");
    switching = true;

    dir = "asc";

    while (switching) {

        switching = false;
        rows = table.rows;

        for (i = 1; i < (rows.length - 1); i++) {

            shouldSwitch = false;

            x = rows[i].getElementsByTagName("TD")[n];
            y = rows[i + 1].getElementsByTagName("TD")[n];

            if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                    shouldSwitch= true;
                    break;
                }
            } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
            switchcount ++;
        } else {

            if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
            }
        }
    }
}





